# Openshift things


## Openshift images

- These are the base images used to build our containers.  They usually come from the redhat container registry. 

### Image Streams used:

- nginx - We will be using this image directly so it will be referenced in the deployment layer. This image: `registry.redhat.io/rhel8/nginx-116`
- springboot-s2i - S2I builder for spring boot apps. There is no openshift image for doing this so I built my own.

### Builds

oc create -f springboot-s2i-bc.yaml            # Builds the s2i image for building spring boot apps
oc create -f spring-hello-is.yaml              # Creates a place to store the spring-hello images
oc create -f spring-hello-bc.yaml              # Builders for spring-hello images 
oc create -f cool-app-dc.yaml                  # How to deploy spring-hello images 
oc create -f cool-app-svc.yaml                 # Grouping of containers into a service  
oc create -f cool-app-route.yaml               # expose port 8080 to point at the cool-app service