# springboot s2i

Openshift does not have a good s2i image for spring boot apps so we need to build our own.

- Based on this: `https://github.com/ganrad/openshift-s2i-springboot-java#testing-the-s2i-builder-image`
- Base image: `pull registry.access.redhat.com/openjdk/openjdk-11-rhel7:1.1`

## How to use

Your git repo needs to contain a gradle based spring boot project (i.e. it builds to a fat jar).  It must have a gradlew script in the top level directory.  This is a bash script that downloads the correct version of gradle and uses it to compile your application.  The built jar is moved to /opt/openshift/app.jar.

## Limitations

- This is kinda slow and the image is kinda big.  Having gradle pre installed would speed up the build a bit but would mean the source code is limited to specific gralde versions.  
- It could also be converted to a multi stage build so the full jdk is not needed in the deployment image. This seems not to be a huge requirement in openshift land.
- There is no current incremental support (can be run with `s2i build --incremental=true springboot-s2i springboot-hello`) This requires the save-artifacts script to be written that was save/load dependencies and whatnot

## Building this image

```
docker build -t springboot-s2i .
```

## Using this image

- With the s2i binary you can run the following from a spring boot source repo:

```
s2i.exe build . springboot-s2i springboot-hello
```

- application is put into `/opt/deployment/app.jar`
- `gradle assemble` is run by default but specifying the `GRADLE_ARGS` environment variable will run other tasks too


## In Openshift

oc new-build --strategy=docker --name=springboot-s2i --context-dir=springboot-s2i https://gitlab.com/DaveStephens/openshift.git
oc logs -f bc/springboot-s2i