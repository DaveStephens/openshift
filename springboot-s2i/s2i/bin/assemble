#!/usr/bin/bash -e

# ID:12-8-2017
#
# S2I assemble script for the 'springboot-s2i' image.
# The 'assemble' script builds your application source ready to run.
#
# For more information refer to the documentation:
#	https://github.com/openshift/source-to-image/blob/master/docs/builder_image.md
#
#HOME directory of base image =>  /opt/app-root/src

execute_gradle_build()
{
  echo "---> Building application using gradlew file ..."

  GRADLE_ARGS="assemble ${GRADLE_ARGS_APPEND}"

  pushd $SRC_DIR

  # Makesure gradlew is executable
  chmod +x ./gradlew

  echo "---> Found gradlew. Attempting to build with: './gradlew -s ${GRADLE_ARGS}'"

  echo "---> Permissions of gradlew: $(ls -al ./gradlew)"
  
  echo "------------------------------"
  echo "Current dir: "
  ls -al 
  echo "------------------------------"  

  # Print the version
  ./gradlew --version

  echo "Building code ...."
  # Execute the actual build
  ./gradlew -s $GRADLE_ARGS

  ERR=$?
  if [ $ERR -ne 0 ]; then
    echo "Aborting due to error code $ERR from Gradle build"
    exit $ERR
  fi

  # Copy built artifacts (if any!) from the builds/libs directory
  # to the $DEPLOY_DIR directory for later deployment
  cp -v $SRC_DIR/build/libs/*.* $DEPLOY_DIR 2> /dev/null

  # Clean up after Gradle run ....
  ./gradlew clean
  popd
  rm -r $SRC_DIR

  if [ -d "$HOME/.gradle" ]; then
    rm -rf $HOME/.gradle
  fi
}

echo "--> S2I:assemble step start ..."
echo "--> Executing script as user=" + `id`

# If the 'springboot-java' assemble script is executed with '-h' flag,
# print the usage.
if [ "$1" = "-h" ]; then
  exec /usr/libexec/s2i/usage
fi

# Restore artifacts from the previous build (if they exist).
if [ "$(ls /tmp/artifacts/ 2>/dev/null)" ]; then
  echo "---> Restoring build artifacts"
  mv /tmp/artifacts/. ./
fi

echo "---> Starting Java web application build process ..."
echo "---> Application source directory is set to $HOME ..."
 
DEPLOY_DIR=/opt/openshift
SRC_DIR=/tmp/src
echo "---> Set target directory to $DEPLOY_DIR ..."

#cp -Rf /tmp/src/. ./
#echo "---> Copied application source to $HOME ..."
ls -la $SRC_DIR

echo "---> S2I:assemble ..."


if [ -f "$SRC_DIR/gradlew" ]; then
  execute_gradle_build
else
  # Copy the fat jar to the deployment directory
  echo "X -> No gradlew file found at top of source directory. Cannot build this image"
  exit 1
fi

# For Spring Boot, there should only be 1 fat jar
if [ $(ls $DEPLOY_DIR/*.jar | wc -l) -eq 1 ]; then
  mv $DEPLOY_DIR/*.jar $DEPLOY_DIR/app.jar
  [ ! -f $DEPLOY_DIR/app.jar ] && echo "Application could not be properly built." && exit 1 
  echo "---> Application deployed successfully.  jar file is located in $DEPLOY_DIR/app.jar"
else
  exit 1
fi